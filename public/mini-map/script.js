const player = document.getElementById('player'),
container = document.getElementById('container'),
body = document.getElementById('body'),
startButton = document.getElementById('startButton'),
main = document.getElementById('main'),
header = document.getElementById('header')

let deg = 0,
keys = new Map(),
pos = {
  x: (body.clientWidth / 2),
  y: (body.clientHeight / 2)
},
forward = 'z', left = 'q', backward = 's', right = 'd',
r = 6, d = 6, ld = d / 2,
limit = 0.5,
latency = 20,
myInterval

container.style.transform = `translate(${pos.x}px, ${pos.y}px)`

function mouseMove(e) { if(e.movementX != 0) deg += r * (e.movementX / Math.abs(e.movementX)) }

function keyMove() {
  if(pos.y < 0 || pos.y > body.clientHeight - player.clientHeight || pos.x < 0 || pos.x > body.clientWidth - player.clientWidth) {
    if(pos.y < 0) pos.y = limit

    if(pos.y > body.clientHeight - player.clientHeight) pos.y = body.clientHeight - limit - player.clientHeight

    if(pos.x < 0) pos.x = limit

    if(pos.x > body.clientWidth - player.clientWidth) pos.x = body.clientWidth - limit - player.clientWidth

    return false
  }
  return true
}

function keyDown(e) { if(!keys.has(e.key)) keys.set(e.key, true) }

function keyUp(e) { keys.delete(e.key) }

function toMove() {

  body.onmousemove = (e) => mouseMove(e)
  document.onkeydown = (e) => keyDown(e)
  document.onkeyup = (e) => keyUp(e)

  if (keys.get(forward) && !keys.get(backward)) {
    if (keyMove()) {
    pos.y -= d * Math.cos(deg * (Math.PI / 180))
    pos.x += d * Math.sin(deg * (Math.PI / 180))
    }
  }

  if (keys.get(backward) && !keys.get(forward)) {
    if (keyMove()) {
    pos.y += d * Math.cos(deg * (Math.PI / 180))
    pos.x -= d * Math.sin(deg * (Math.PI / 180))
    }
  }

  if (keys.get(left) && !keys.get(right)) {
    if (keyMove()) {
    pos.y -= ld * Math.cos((deg - 90) * (Math.PI / 180))
    pos.x += ld * Math.sin((deg - 90) * (Math.PI / 180))
    }
  }

  if (keys.get(right) && !keys.get(left)) {
    if (keyMove()) {
    pos.y -= ld * Math.cos((deg + 90) * (Math.PI / 180))
    pos.x += ld * Math.sin((deg + 90) * (Math.PI / 180))
    }
  }

  container.style.transform = `translate(${pos.x}px, ${pos.y}px)`
  player.style.transform = `rotateZ(${deg}deg)`

}

body.onclick = () => body.requestFullscreen()

startButton.onclick = () => {
  header.style.display = 'none'
  main.style.display = 'initial'
  body.requestPointerLock()
}

document.onpointerlockchange = () => {

  if (document.pointerLockElement === body) myInterval = setInterval(toMove, latency)

  else {
    header.style.display = null
    main.style.display = null
    clearInterval(myInterval)
  }
}