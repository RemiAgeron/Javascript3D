function cl(x) { console.log(x) }

const room = document.getElementById('room'),
  player = document.getElementById('player')

  let i = 1,
  pos = 0,
  deg = 360,
  origin = { x: 0, y: 0, z: 0 }

document.addEventListener('keydown', (event) => {
  switch (event.key) {
    case 'ArrowRight':
      room.style.transformOrigin = `${origin.x}px ${origin.y}px ${origin.z}px`
      deg += i
      room.style.transform = `rotateY(${deg}deg)`
      break;
    
    case 'ArrowLeft':
      room.style.transformOrigin = `${origin.x}px ${origin.y}px ${origin.z}px`
      deg = deg == 0 ? 360 - i : deg -= i
      room.style.transform = `rotateY(${deg}deg)`
      break;
      
    case 'ArrowUp':
      if (pos < 1000) {
        pos += 20 * i
        origin.z -= 20 * i
        player.style.transform = `translateZ(${pos}px)`
      } else pos = 1000
      break;
    
    case 'ArrowDown':
      if (pos > -1000) {
        pos -= 20 * i
        origin.z += 20 * i
        player.style.transform = `translateZ(${pos}px)`
      }
      break;
  }
  cl(origin)
})